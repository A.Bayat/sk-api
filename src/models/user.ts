import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";
import { Iuser } from "../interfaces/User";

@Entity()
export class User implements Iuser {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({
    length: 100
  })
  firstName: string;
  @Column({
    length: 100
  })
  lastName: string;
  @Column()
  email: string;
  @Column()
  displayName: string;

  constructor(
    firstName: string,
    lastName: string,
    email: string,
    password: string,
    id: number
  ) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.displayName = password;
    this.id = id;
  }
  fullName = () => {
    return `${this.firstName} ${this.lastName}`;
  };
}
