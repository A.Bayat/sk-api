import cluster from "cluster";
import express, { Application, Request, Response, NextFunction } from "express";
import bodyParser from "body-parser";
import { createConnection, Connection } from "typeorm";
import { User } from "./entity/user";
import { AppRouter } from "./AppRouter";
const os = require("os");

import "./controllers/LoginController";
import "./controllers/SignUpController";
const cpuCount: Number = os.cpus().length;

const port = 3003;
createConnection()
  .then(Connection => {
    console.log("Database connected");
    const app: Application = express();
    app.use(bodyParser.urlencoded({ extended: true }), bodyParser.json());
    app.use(AppRouter.getInstance());
    app.listen(process.env.PORT || port, () =>
      console.log(
        `Server running on port ${port} number of available cpu is ${cpuCount}`
      )
    );
  })
  .catch(error => {
    console.log(error);
  });
