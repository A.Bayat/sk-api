export enum Methods {
  Get = "get",
  Post = "post",
  Del = "delete",
  Patch = "patch",
  Put = "put"
}
