import { Request, Response, NextFunction } from "express";
import { controller } from "./decorators/controller";
import { get, post } from "./decorators/routes";
import { use } from "./decorators/use";
import { bodyValidator } from "./decorators/bodyValidator";

function logger(req: Request, res: Response, next: NextFunction) {
  console.log("A middleware was called!!");
  next();
}

@controller("")
class LoginController {
  @use(logger)
  @get("/login")
  getLogin(req: Request, res: Response, next: NextFunction): void {
    res.send(`
      <form method="POST">
        <div>
          <label>Email</label>
          <input name="email" />
        </div>
        <div>
          <label>Password</label>
          <input name="password" type="password" />
        </div>
        <button>Submit</button>
      </form>
    `);
  }
  @post("/login")
  @bodyValidator("email", "password")
  postLogin(req: Request, res: Response, next: NextFunction): void {
    const { email, password } = req.body;
    console.log(`email: ${email} password: ${password}`);
    next();
  }
}
