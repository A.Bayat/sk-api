import "reflect-metadata";
import { MetadataKeys } from "../../enums/MetadataKeys";
import { RequestHandler } from "express";

export function bodyValidator(...keys: string[]) {
  return function(
    target: any,
    key: string,
    desc: TypedPropertyDescriptor<RequestHandler>
  ) {
    Reflect.defineMetadata(MetadataKeys.Validator, keys, target, key);
  };
}
