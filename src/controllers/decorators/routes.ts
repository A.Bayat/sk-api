import "reflect-metadata";
import { Methods } from "../../enums/Methods";
import { MetadataKeys } from "../../enums/MetadataKeys";

function routesBinder(method: string) {
  return function(path: string) {
    return function(
      target: any,
      key: string,
      desc: TypedPropertyDescriptor<any>
    ) {
      Reflect.defineMetadata(MetadataKeys.Path, path, target, key);
      Reflect.defineMetadata(MetadataKeys.Method, method, target, key);
    };
  };
}

export const get = routesBinder(Methods.Get);
export const put = routesBinder(Methods.Put);
export const post = routesBinder(Methods.Post);
export const del = routesBinder(Methods.Del);
export const patch = routesBinder(Methods.Patch);
