import "reflect-metadata";
import { AppRouter } from "../../AppRouter";
import { Methods } from "../../enums/Methods";
import { MetadataKeys } from "../../enums/MetadataKeys";
import { Request, Response, NextFunction, RequestHandler } from "express";

function bodyValidators(keys: string): RequestHandler {
  return function(req: Request, res: Response, next: NextFunction): void {
    if (!req.body) {
      res.status(422).send("Invalid body");
      return;
    }
    for (let key of keys) {
      if (!req.body[key]) {
        res.status(422).send(`Missing ${key}`);
        return;
      }
    }
    next();
  };
}

export function controller(routerPrefix: string) {
  return function(target: Function) {
    const router = AppRouter.getInstance();

    Object.getOwnPropertyNames(target.prototype).forEach(key => {
      const routHandler = target.prototype[key];

      const path: MetadataKeys = Reflect.getMetadata(
        MetadataKeys.Path,
        target.prototype,
        key
      );

      const method: Methods = Reflect.getMetadata(
        MetadataKeys.Method,
        target.prototype,
        key
      );

      const middlewares =
        Reflect.getMetadata(MetadataKeys.Middleware, target.prototype, key) ||
        [];

      const requiredBodyProps =
        Reflect.getMetadata(MetadataKeys.Validator, target.prototype, key) ||
        [];
      const validator = bodyValidators(requiredBodyProps);
      if (path) {
        const endPoint = `${routerPrefix}${path}`;
        router[method](endPoint, ...middlewares, validator, routHandler);
      }
    });
  };
}
