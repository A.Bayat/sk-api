import { getRepository, Repository } from "typeorm";
import { Request, Response, NextFunction, json } from "express";
import { controller } from "./decorators/controller";
import { get, post } from "./decorators/routes";
import { User } from "../entity/user";
import { Iuser } from "../interfaces/User";

@controller("/sign-up")
class SignUpController {
  @get("/")
  signUpPage(
    req: Request & { user: User },
    res: Response,
    next: NextFunction
  ): void {
    res.send(`<h3>{user.displayName}</h3>`);
  }

  @post("/")
  async createUser(req: Request, res: Response, next: NextFunction) {
    try {
      if (
        !getRepository<User>(User) ||
        !(await getRepository(User).findOne({
          email: req.body.email,
        }))
      ) {
        const u = new User();
        u.displayName = req.body.displayName;
        u.email = req.body.email;
        u.firstName = req.body.firstName;
        u.lastName = req.body.lastName;
        getRepository<User>(User).create(u);
        await getRepository<User>(User).save<User>(u);

        res.status(201).json(`user ${u.fullName()} created`);
      } else {
        throw new Error("There is a record with this email");
      }
    } catch (err) {
      res.json({ Error: "Something went wrong", message: err.message });
    }
  }
}
