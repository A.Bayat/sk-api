export interface Iuser {
  firstName: string;
  lastName: string;
  email: string;
  displayName: string;
  fullName(): string;
}
