import { AfterInsert, Entity, Column, PrimaryGeneratedColumn } from "typeorm";
import { Iuser } from "../interfaces/User";

@Entity()
export class User implements Iuser {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({
    length: 100
  })
  firstName!: string;

  @Column({
    length: 100
  })
  lastName!: string;

  @Column()
  email!: string;

  @Column()
  displayName!: string;

  @AfterInsert()
  fullName = () => {
    console.log(`${this.firstName} ${this.lastName}`);
    return `${this.firstName} ${this.lastName}`;
  };
}
